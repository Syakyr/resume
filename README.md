The Resume of Syakyr Surani
=========================

This resume uses [Deedy Resume](https://github.com/deedy/Deedy-Resume) OpenFont variant as the template.

The font family and `xelatex` are required to be installed to generate the pdf.

If basicTEX is used, these modules are also required to be installed using `tlmgr`:
- `textpos`
- `isodate`
- `substr`
- `titlesec`
- `babel`

To generate the pdf:  
`xelatex syakyr_resume.xtx`

It is licensed under the Apache License 2.0.

The PDF version can be found [here](syakyr_resume.pdf).

![](syakyr_resume.png)
